package pl.course.tomek;

public class Main {

    public static void main(String[] args) {
        int[] myIntegers = Array.getIntegers(5);
        Array.sort(myIntegers);
        Array.printArray(myIntegers);
    }
}
